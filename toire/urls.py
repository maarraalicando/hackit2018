from django.urls import path, re_path, include

from . import views

app_name = 'toire'
urlpatterns = [
    path('', views.index, name='index'),
    # re_path(r'^convert/', include('lazysignup.urls')),
    path('ex', views.export_logs, name='export_logs'),
    path('allLogs/', views.allLogs, name="allLogs"),
    path('guest/', views.guest_test, name="guest"),
]