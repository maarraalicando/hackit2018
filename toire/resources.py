from import_export import resources
from .models import Record

class RecordResource(resources.ModelResource):
    class Meta:
        model = Record
        fields = ('time', 'note',)
        export_order = ('time', 'note',)