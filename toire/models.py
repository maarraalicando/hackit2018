from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Record(models.Model):
    note = models.CharField(max_length = 1000)
    date = models.DateField()
    time = models.DateTimeField()
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    is_anonymous = models.BooleanField(default=False)

    def __str__(self):
    	return self.note

    def as_json(self):
        return dict(
            note=self.note, 
            time=self.time.isoformat(),
            )