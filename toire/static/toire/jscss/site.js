/**
 * Created by User on 11/10/18.
 */

$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
    $("#menu-toggle").hide();
});

$("#menu-toggle2").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
    $("#menu-toggle").show();
});

var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
var dayNames= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]

var newDate = new Date();
newDate.setDate(newDate.getDate());
$('#Date').html(dayNames[newDate.getDay()] + ", "  + monthNames[newDate.getMonth()] + " " + newDate.getDate() + ', ' + newDate.getFullYear());

setInterval( function() {
    var seconds = new Date().getSeconds();
    $("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
},1000);

setInterval( function() {
    var minutes = new Date().getMinutes();
    $("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
},1000);

setInterval( function() {
    var hours = new Date().getHours();
    $("#hours").html(( hours < 10 ? "0" : "" ) + hours);
}, 1000);


/*ALL LOGS*/

$('#tblLogs').DataTable( {
    "processing": true,
    "ajax": {
        "processing": true,
        "url": "/watch/ex",
        "dataSrc": ""
    },

    "columns": [
        { "data": "time" },
        { "data": "note" }
    ]
} );


/*ALL LOGS*/