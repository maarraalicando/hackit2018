from lazysignup.decorators import allow_lazy_user

from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import redirect
from .resources import RecordResource
from datetime import datetime
from .models import Record

from .forms import NoteForm

from lazysignup.templatetags.lazysignup_tags import is_lazy_user

# Create your views here.
@allow_lazy_user
def index(request):
    if request.method == 'POST':
        r = Record(note = request.POST.get('notes'), date = datetime.today(), time = datetime.now(), user_id = request.user, is_anonymous=is_lazy_user(request.user))

        r.save()
    r = Record.objects.filter(user_id = request.user).order_by('-id')[0]
    ar = Record.objects.filter(user_id = request.user).order_by('-id')
    return render(request, 'toire/index.html', context = {'records':r, 'arecods':ar})

@allow_lazy_user
def export_logs(request):
    context = {

    }
    record_resource = RecordResource()
    queryset = Record.objects.filter(user_id = request.user)
    dataset = record_resource.export(queryset)
    response = HttpResponse(dataset.json, content_type='application/json')
    response['Content-Disposition'] = 'attachment; filename="records.json"'
    return response

@allow_lazy_user
def allLogs(request):
    context = {

    }
    return render(request, 'toire/allLogs.html', context)

@allow_lazy_user
def guest_test(request):
    # attrs = {}
    # for k, v in request.user:
    #     attrs[k] = v

    context = {
        'username' : request.user.username,
        'user_id' : request.user.id,
        'is_lazy' : is_lazy_user(request.user)
        # 'attrs': attrs
    }
    
    return render(request, 'toire/dummy_index.html', context)