from django import forms

class NoteForm(forms.Form):
    log_notes = forms.CharField(label='Notes', max_length=200)